from django.shortcuts import render
from django.http import JsonResponse
from .models import Post

def index(request):
    posts = Post.objects.all().values()
    return JsonResponse({'posts': list(posts)})

def show(request, id):
    post = Post.objects.get(id=id).values()
    return JsonResponse({'post': list(post)[0]})